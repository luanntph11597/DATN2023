﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class BrandRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public BrandRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Brand findById(int id)
        {

            var brand = _context.Brands.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return brand;
        }
        public List<Brand> findAll()
        {
            var brand = _context.Brands.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return brand;
        }
    }
}
