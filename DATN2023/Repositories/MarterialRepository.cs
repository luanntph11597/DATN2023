﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class MarterialRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public MarterialRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Marterial findById(int id)
        {
            var marterial = _context.Marterials.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return marterial;
        }
        public List<Marterial> findAll()
        {
            var marterial = _context.Marterials.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return marterial;
        }
    }
}
