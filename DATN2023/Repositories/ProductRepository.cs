﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Dtos.ProductDtos;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class ProductRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public ProductRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public List<Product> Search(string keyword, decimal? minPrice, decimal? maxPrice, ProductSearchInputDto input)
        {
            var query = _context.Products.AsQueryable();

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(p => (p.Name.Contains(keyword) || p.Description.Contains(keyword)) && p.Status == Enumirations.Status.ACTIVE.GetHashCode());
            }

            if (minPrice.HasValue)
            {
                query = query.Where(p => p.Price >= minPrice.Value);
            }

            if (maxPrice.HasValue)
            {
                query = query.Where(p => p.Price <= maxPrice.Value);
            }

            if (input.AverageRating.HasValue && input.AverageRating != 0)
            {
                query = query.Where(p => p.AverageRating == input.AverageRating.Value);
            }

            if (input.CategoryId.HasValue && input.CategoryId != 0)
            {
                query = query.Where(p => p.CategoryId == input.CategoryId.Value);
            }
            if (input.BrandId.HasValue && input.BrandId != 0)
            {
                query = query.Where(p => p.BrandId == input.BrandId.Value);
            }
            if (input.ColorId.HasValue && input.ColorId != 0)
            {
                query = query.Where(p => p.ColorId == input.ColorId.Value);
            }
            if (input.SizeId.HasValue && input.SizeId != 0)
            {
                query = query.Where(p => p.SizeId == input.SizeId.Value);
            }
            if (input.SupplierId.HasValue && input.SupplierId != 0)
            {
                query = query.Where(p => p.SupplierId == input.SupplierId.Value);
            }
            if (input.GenderId.HasValue && input.GenderId != 0)
            {
                query = query.Where(p => p.GenderId == input.GenderId.Value);
            }
            if (input.ProductTypeId.HasValue && input.ProductTypeId != 0)
            {
                query = query.Where(p => p.GenderId == input.ProductTypeId.Value);
            }
            if (input.MarterialId.HasValue && input.MarterialId != 0)
            {
                query = query.Where(p => p.GenderId == input.MarterialId.Value);
            }
            if (input.Status.HasValue)
            {
                query = query.Where(p => p.Status == input.Status.Value);
            }
            return query.ToList();
        }
    }
}
