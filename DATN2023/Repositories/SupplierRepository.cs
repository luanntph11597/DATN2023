﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class SupplierRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public SupplierRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Supplier findById(int id)
        {
            var supplier = _context.Suppliers.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return supplier;
        }
        public List<Supplier> findAll()
        {
            var supplier = _context.Suppliers.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return supplier;
        }
    }
}
