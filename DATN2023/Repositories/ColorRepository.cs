﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class ColorRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public ColorRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Color findById(int id)
        {
            var color = _context.Colors.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return color;
        }
        public List<Color> findAll()
        {
            var color = _context.Colors.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return color;
        }
    }
}
