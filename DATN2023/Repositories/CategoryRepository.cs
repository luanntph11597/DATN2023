﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class CategoryRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public CategoryRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Category findById(int id)
        {
            var category = _context.Categories.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return category;
        }

        public List<Category> findAll()
        {
            var category = _context.Categories.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return category;
        }
    }
}
