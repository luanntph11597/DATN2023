﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class ProductTypeRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public ProductTypeRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public ProductType findById(int id)
        {
            var productType = _context.ProductTypes.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return productType;
        }
        public List<ProductType> findAll()
        {
            var productType = _context.ProductTypes.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return productType;
        }
    }
}
