﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class SizeRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public SizeRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Size findById(int id)
        {
            var size = _context.Sizes.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return size;
        }
        public List<Size> findAll()
        {
            var size = _context.Sizes.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return size;
        }
    }
}
