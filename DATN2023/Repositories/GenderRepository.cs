﻿using AutoMapper;
using DATN2023.DBContext;
using DATN2023.Models;
using System.Collections.Generic;
using System.Linq;

namespace DATN2023.Repositories
{
    public class GenderRepository
    {
        private readonly DATN2023DBContext _context;

        private readonly IMapper _mapper;

        public GenderRepository(DATN2023DBContext context,
            IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public Gender findById(int id)
        {
            var gender = _context.Genders.Where(x => x.Id == id && x.Status == Enumirations.Status.ACTIVE.GetHashCode()).FirstOrDefault();
            return gender;
        }
        public List<Gender> findAll()
        {
            var gender = _context.Genders.Where(x => x.Status == Enumirations.Status.ACTIVE.GetHashCode()).ToList();
            return gender;
        }
    }
}
