﻿using DATN2023.BaseModel;
using DATN2023.Dtos.CategoryDtos;
using DATN2023.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Interfaces
{
    public interface ICategoryService
    {
        Task<BaseResponseModel<Category>> CreateAsync(CreateUpdateCategoryInputDto input);
        Task<BaseResponseModel<Category>> UpdateAsync(int id, CreateUpdateCategoryInputDto input);
        Task<BaseResponseModel<List<Category>>> GetListAsync(SearchCategoryDto input);
        Task<BaseResponseModel<Category>> GetAsync(int id);
        Task<BaseResponseModel<Category>> DeleteAsync(int id);
    }
}
