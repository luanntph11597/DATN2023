﻿using DATN2023.BaseModel;
using DATN2023.Dtos.MarterialDtos;
using DATN2023.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Interfaces
{
    public interface IMarterialService
    {
        Task<BaseResponseModel<Marterial>> CreateAsync(CreateUpdateMarterialInputDto input);
        Task<BaseResponseModel<Marterial>> UpdateAsync(int id, CreateUpdateMarterialInputDto input);
        Task<BaseResponseModel<List<Marterial>>> GetListAsync(SearchMarterialDto input);
        Task<BaseResponseModel<Marterial>> GetAsync(int id);
        Task<BaseResponseModel<Marterial>> DeleteAsync(int id);
    }
}
