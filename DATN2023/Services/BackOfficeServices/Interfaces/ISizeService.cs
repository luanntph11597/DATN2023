﻿using DATN2023.BaseModel;
using DATN2023.Dtos.SizeDtos;
using DATN2023.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Interfaces
{
    public interface ISizeService
    {
        Task<BaseResponseModel<Size>> CreateAsync(CreateUpdateSizeInputDto input);
        Task<BaseResponseModel<Size>> UpdateAsync(int id, CreateUpdateSizeInputDto input);
        Task<BaseResponseModel<List<Size>>> GetListAsync(SearchSizeDto input);
        Task<BaseResponseModel<Size>> GetAsync(int id);
        Task<BaseResponseModel<Size>> DeleteAsync(int id);
    }
}
