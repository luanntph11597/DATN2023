﻿using DATN2023.BaseModel;
using DATN2023.Dtos.ColorDtos;
using DATN2023.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Interfaces
{
    public interface IColorService
    {
        Task<BaseResponseModel<Color>> CreateAsync(CreateUpdateColorInputDto input);
        Task<BaseResponseModel<Color>> UpdateAsync(int id, CreateUpdateColorInputDto input);
        Task<BaseResponseModel<List<Color>>> GetListAsync(SearchColorDto input);
        Task<BaseResponseModel<Color>> GetAsync(int id);
        Task<BaseResponseModel<Color>> DeleteAsync(int id);
    }
}
