﻿using DATN2023.BaseModel;
using DATN2023.Dtos.SupplierDtos;
using DATN2023.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Interfaces
{
    public interface ISupplierService
    {
        Task<BaseResponseModel<Supplier>> CreateAsync(CreateUpdateSupplierInputDto input);
        Task<BaseResponseModel<Supplier>> UpdateAsync(int id, CreateUpdateSupplierInputDto input);
        Task<BaseResponseModel<List<Supplier>>> GetListAsync(SearchSupplierDto input);
        Task<BaseResponseModel<Supplier>> GetAsync(int id);
        Task<BaseResponseModel<Supplier>> DeleteAsync(int id);
    }
}
