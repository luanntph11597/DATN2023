﻿using DATN2023.BaseModel;
using DATN2023.DBContext;
using DATN2023.Dtos.ColorDtos;
using DATN2023.Enumirations;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Implements
{
    public class ColorService : IColorService
    {
        private readonly DATN2023DBContext _context;
        public ColorService(DATN2023DBContext context)
        {
            _context = context;
        }
        public async Task<BaseResponseModel<Color>> CreateAsync(CreateUpdateColorInputDto input)
        {
            var colorNameDuplicate = _context.Colors.FirstOrDefault(x => x.Name.ToLower() == input.ColorName.ToLower() &&
                                                             x.Status == Status.ACTIVE.GetHashCode());
            if (colorNameDuplicate != null)
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.ColorName}"
                };
            }

            var colorCodeDuplicate = _context.Colors.FirstOrDefault(x => x.Code.ToLower() == input.ColorCode.ToLower() &&
                                                  x.Status == Status.ACTIVE.GetHashCode());
            if (colorCodeDuplicate != null)
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.ColorCode}"
                };
            }

            var entity = new Color
            {
                Name = input.ColorName,
                Code = input.ColorCode,
                Status = Status.ACTIVE.GetHashCode(),
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return new BaseResponseModel<Color>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Thêm mới bản ghi thành công",
                Data = entity
            };
        }

        public async Task<BaseResponseModel<Color>> DeleteAsync(int id)
        {
            var color = await _context.Colors.FirstOrDefaultAsync(x => x.Id == id);
            if (color == null)
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            if (_context.Products.Any(x => x.ColorId == color.Id))
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không thể xóa vì màu đang được sử dụng trong sản phẩm"
                };
            }

            color.Status = Status.DELETED.GetHashCode();
            _context.SaveChanges();
            return new BaseResponseModel<Color>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = $"Xóa bản ghi thành công"
            };
        }

        public async Task<BaseResponseModel<Color>> GetAsync(int id)
        {
            var color = await _context.Colors.FirstOrDefaultAsync(x => x.Id == id);
            if (color == null)
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            return new BaseResponseModel<Color>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Success",
                Data = color
            };
        }

        public async Task<BaseResponseModel<List<Color>>> GetListAsync(SearchColorDto input)
        {
            var query = _context.Colors.AsQueryable().Where(x => x.Status == Status.ACTIVE.GetHashCode());
            if (!string.IsNullOrEmpty(input.ColorName))
            {
                query = query.Where(x => x.Name == input.ColorName);
            }
            if (!string.IsNullOrEmpty(input.ColorCode))
            {
                query = query.Where(x => x.Code == input.ColorCode);
            }

            var data = await query.OrderByDescending(x => x.Id)
            .Skip((input.PageNumber) * input.PageSize)
            .Take(input.PageSize)
            .ToListAsync();

            return new BaseResponseModel<List<Color>>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Data = data
            };
        }

        public async Task<BaseResponseModel<Color>> UpdateAsync(int id, CreateUpdateColorInputDto input)
        {
            var colorDuplicate = _context.Colors.FirstOrDefault(x => x.Id == id);
            if (colorDuplicate == null)
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }

            if (_context.Colors.Any(x => x.Name.ToLower() == input.ColorName.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.ColorName}"
                };
            }

            if (_context.Colors.Any(x => x.Code.ToLower() == input.ColorCode.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Color>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.ColorCode}"
                };
            }

            colorDuplicate.Code = input.ColorCode;
            colorDuplicate.Name = input.ColorName;
            colorDuplicate.Status = Status.ACTIVE.GetHashCode();

            await _context.SaveChangesAsync();
            return new BaseResponseModel<Color>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Cập nhật bản ghi thành công",
                Data = colorDuplicate
            };
        }
    }
}
