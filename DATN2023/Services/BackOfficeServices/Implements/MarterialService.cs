﻿using DATN2023.BaseModel;
using DATN2023.DBContext;
using DATN2023.Dtos.MarterialDtos;
using DATN2023.Enumirations;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Implements
{
    public class MarterialService : IMarterialService
    {
        private readonly DATN2023DBContext _context;
        public MarterialService(DATN2023DBContext context)
        {
            _context = context;
        }

        public async Task<BaseResponseModel<Marterial>> CreateAsync(CreateUpdateMarterialInputDto input)
        {
            var marterialNameDuplicate = _context.Marterials.FirstOrDefault(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                             x.Status == Status.ACTIVE.GetHashCode());
            if (marterialNameDuplicate != null)
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            var marterialCodeDuplicate = _context.Marterials.FirstOrDefault(x => x.Code.ToLower() == input.Code.ToLower() &&
                                                  x.Status == Status.ACTIVE.GetHashCode());
            if (marterialCodeDuplicate != null)
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            var entity = new Marterial
            {
                Name = input.Name,
                Code = input.Code,
                Description = input.Description,
                Status = Status.ACTIVE.GetHashCode(),
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return new BaseResponseModel<Marterial>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Thêm mới bản ghi thành công",
                Data = entity
            };
        }

        public async Task<BaseResponseModel<Marterial>> DeleteAsync(int id)
        {
            var marterial = await _context.Marterials.FirstOrDefaultAsync(x => x.Id == id);
            if (marterial == null)
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            if (_context.Products.Any(x => x.MarterialId == marterial.Id))
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không thể xóa vì đang được sử dụng trong sản phẩm"
                };
            }

            marterial.Status = Status.DELETED.GetHashCode();
            _context.SaveChanges();
            return new BaseResponseModel<Marterial>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = $"Xóa bản ghi thành công"
            };
        }

        public async Task<BaseResponseModel<Marterial>> GetAsync(int id)
        {
            var marterial = await _context.Marterials.FirstOrDefaultAsync(x => x.Id == id);
            if (marterial == null)
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            return new BaseResponseModel<Marterial>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Success",
                Data = marterial
            };
        }

        public async Task<BaseResponseModel<List<Marterial>>> GetListAsync(SearchMarterialDto input)
        {
            var query = _context.Marterials.AsQueryable().Where(x => x.Status == Status.ACTIVE.GetHashCode());
            if (!string.IsNullOrEmpty(input.Name))
            {
                query = query.Where(x => x.Name == input.Name);
            }
            if (!string.IsNullOrEmpty(input.Code))
            {
                query = query.Where(x => x.Code == input.Code);
            }

            var data = await query.OrderByDescending(x => x.Id)
            .Skip((input.PageNumber) * input.PageSize)
            .Take(input.PageSize)
            .ToListAsync();

            return new BaseResponseModel<List<Marterial>>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Data = data
            };
        }

        public async Task<BaseResponseModel<Marterial>> UpdateAsync(int id, CreateUpdateMarterialInputDto input)
        {
            var marterial = _context.Marterials.FirstOrDefault(x => x.Id == id);
            if (marterial == null)
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }

            if (_context.Marterials.Any(x => x.Name.ToLower() == input.Name.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            if (_context.Marterials.Any(x => x.Code.ToLower() == input.Code.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Marterial>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            marterial.Code = input.Code;
            marterial.Name = input.Name;
            marterial.Description = input.Description;
            marterial.Status = Status.ACTIVE.GetHashCode();

            await _context.SaveChangesAsync();
            return new BaseResponseModel<Marterial>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Cập nhật bản ghi thành công",
                Data = marterial
            };
        }
    }
}
