﻿using DATN2023.BaseModel;
using DATN2023.DBContext;
using DATN2023.Dtos.CategoryDtos;
using DATN2023.Enumirations;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Implements
{
    public class CategoryService : ICategoryService
    {
        private readonly DATN2023DBContext _context;
        public CategoryService(DATN2023DBContext context)
        {
            _context = context;
        }

        public async Task<BaseResponseModel<Category>> CreateAsync(CreateUpdateCategoryInputDto input)
        {
            var categoryNameDuplicate = _context.Categories.FirstOrDefault(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                             x.Status == Status.ACTIVE.GetHashCode());
            if (categoryNameDuplicate != null)
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            var categoryCodeDuplicate = _context.Categories.FirstOrDefault(x => x.Code.ToLower() == input.Code.ToLower() &&
                                                  x.Status == Status.ACTIVE.GetHashCode());
            if (categoryCodeDuplicate != null)
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            var entity = new Category
            {
                Name = input.Name,
                Code = input.Code,
                Description = input.Description,
                Status = Status.ACTIVE.GetHashCode(),
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return new BaseResponseModel<Category>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Thêm mới bản ghi thành công",
                Data = entity
            };
        }

        public Task<BaseResponseModel<Category>> DeleteAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<BaseResponseModel<Category>> GetAsync(int id)
        {
            var category = await _context.Categories.FirstOrDefaultAsync(x => x.Id == id);
            if (category == null)
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            return new BaseResponseModel<Category>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Success",
                Data = category
            };
        }

        public async Task<BaseResponseModel<List<Category>>> GetListAsync(SearchCategoryDto input)
        {
            var query = _context.Categories.AsQueryable().Where(x => x.Status == Status.ACTIVE.GetHashCode());
            if (!string.IsNullOrEmpty(input.Name))
            {
                query = query.Where(x => x.Name == input.Name);
            }
            if (!string.IsNullOrEmpty(input.Code))
            {
                query = query.Where(x => x.Code == input.Code);
            }

            var data = await query.OrderByDescending(x => x.Id)
            .Skip((input.PageNumber) * input.PageSize)
            .Take(input.PageSize)
            .ToListAsync();

            return new BaseResponseModel<List<Category>>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Data = data
            };
        }

        public async Task<BaseResponseModel<Category>> UpdateAsync(int id, CreateUpdateCategoryInputDto input)
        {
            var category = _context.Categories.FirstOrDefault(x => x.Id == id);
            if (category == null)
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }

            if (_context.Categories.Any(x => x.Name.ToLower() == input.Name.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            if (_context.Categories.Any(x => x.Code.ToLower() == input.Code.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Category>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            category.Code = input.Code;
            category.Name = input.Name;
            category.Description = input.Description;
            category.Status = Status.ACTIVE.GetHashCode();

            await _context.SaveChangesAsync();
            return new BaseResponseModel<Category>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Cập nhật bản ghi thành công",
                Data = category
            };
        }
    }
}
