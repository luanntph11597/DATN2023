﻿using DATN2023.BaseModel;
using DATN2023.DBContext;
using DATN2023.Dtos.SizeDtos;
using DATN2023.Enumirations;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Implements
{
    public class SizeService : ISizeService
    {
        private readonly DATN2023DBContext _context;
        public SizeService(DATN2023DBContext context)
        {
            _context = context;
        }
        public async Task<BaseResponseModel<Size>> CreateAsync(CreateUpdateSizeInputDto input)
        {
            var sizeNameDuplicate = _context.Sizes.FirstOrDefault(x => x.Name.ToLower() == input.SizeName.ToLower() &&
                                                              x.Status == Status.ACTIVE.GetHashCode());
            if (sizeNameDuplicate != null)
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.SizeName}"
                };
            }

            var sizeCodeDuplicate = _context.Sizes.FirstOrDefault(x => x.Code.ToLower() == input.SizeCode.ToLower() &&
                                                  x.Status == Status.ACTIVE.GetHashCode());
            if (sizeCodeDuplicate != null)
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.SizeCode}"
                };
            }

            var entity = new Size
            {
                Name = input.SizeName,
                Code = input.SizeCode,
                Status = Status.ACTIVE.GetHashCode(),
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return new BaseResponseModel<Size>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Thêm mới bản ghi thành công",
                Data = entity
            };
        }

        public async Task<BaseResponseModel<Size>> DeleteAsync(int id)
        {
            var size = await _context.Sizes.FirstOrDefaultAsync(x => x.Id == id);
            if (size == null)
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            if(_context.Products.Any(x => x.SizeId == size.Id))
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không thể xóa vì kích cỡ đang được sử dụng trong sản phẩm"
                };
            }

            size.Status = Status.DELETED.GetHashCode();
            _context.SaveChanges();
            return new BaseResponseModel<Size>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = $"Xóa bản ghi thành công"
            };
        }

        public async Task<BaseResponseModel<Size>> GetAsync(int id)
        {
            var size = await _context.Sizes.FirstOrDefaultAsync(x => x.Id == id);
            if (size == null)
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            return new BaseResponseModel<Size>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Success",
                Data = size
            };
        }

        public async Task<BaseResponseModel<List<Size>>> GetListAsync(SearchSizeDto input)
        {
            var query = _context.Sizes.AsQueryable().Where(x => x.Status == Status.ACTIVE.GetHashCode());
            if (!string.IsNullOrEmpty(input.SizeName))
            {
               query = query.Where(x => x.Name == input.SizeName);
            }
            if (!string.IsNullOrEmpty(input.SizeCode))
            {
               query = query.Where(x => x.Code == input.SizeCode);
            }

            var data = await query.OrderByDescending(x => x.Id)
            .Skip((input.PageNumber) * input.PageSize)
            .Take(input.PageSize)
            .ToListAsync();

            return new BaseResponseModel<List<Size>>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Data = data
            };
        }

        public async Task<BaseResponseModel<Size>> UpdateAsync(int id, CreateUpdateSizeInputDto input)
        {
            var sizeDuplicate = _context.Sizes.FirstOrDefault(x => x.Id == id);
            if (sizeDuplicate == null)
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }

            if (_context.Sizes.Any(x => x.Name.ToLower() == input.SizeName.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.SizeName}"
                };
            }

            if (_context.Sizes.Any(x => x.Code.ToLower() == input.SizeCode.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Size>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.SizeCode}"
                };
            }

            sizeDuplicate.Code = input.SizeCode;
            sizeDuplicate.Name = input.SizeName;
            sizeDuplicate.Status = Status.ACTIVE.GetHashCode();

            await _context.SaveChangesAsync();
            return new BaseResponseModel<Size>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Cập nhật bản ghi thành công",
                Data = sizeDuplicate
            };
        }
    }
}
