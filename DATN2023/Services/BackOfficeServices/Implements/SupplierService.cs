﻿using DATN2023.BaseModel;
using DATN2023.DBContext;
using DATN2023.Dtos.SupplierDtos;
using DATN2023.Enumirations;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.EntityFrameworkCore;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DATN2023.Services.BackOfficeServices.Implements
{
    public class SupplierService : ISupplierService
    {
        private readonly DATN2023DBContext _context;
        public SupplierService(DATN2023DBContext context)
        {
            _context = context;
        }

        public async Task<BaseResponseModel<Supplier>> CreateAsync(CreateUpdateSupplierInputDto input)
        {
            var supplierNameDuplicate = _context.Suppliers.FirstOrDefault(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                             x.Status == Status.ACTIVE.GetHashCode());
            if (supplierNameDuplicate != null)
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            var SupplierCodeDuplicate = _context.Suppliers.FirstOrDefault(x => x.Code.ToLower() == input.Code.ToLower() &&
                                                  x.Status == Status.ACTIVE.GetHashCode());
            if (SupplierCodeDuplicate != null)
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            var entity = new Supplier
            {
                Name = input.Name,
                Code = input.Code,
                Status = Status.ACTIVE.GetHashCode(),
            };
            _context.Add(entity);
            await _context.SaveChangesAsync();

            return new BaseResponseModel<Supplier>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Thêm mới bản ghi thành công",
                Data = entity
            };
        }

        public async Task<BaseResponseModel<Supplier>> DeleteAsync(int id)
        {
            var supplier = await _context.Suppliers.FirstOrDefaultAsync(x => x.Id == id);
            if (supplier == null)
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            if (_context.Products.Any(x => x.SupplierId == supplier.Id))
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không thể xóa vì nhà cung cấp đang được sử dụng trong sản phẩm"
                };
            }

            supplier.Status = Status.DELETED.GetHashCode();
            _context.SaveChanges();
            return new BaseResponseModel<Supplier>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = $"Xóa bản ghi thành công"
            };
        }

        public async Task<BaseResponseModel<Supplier>> GetAsync(int id)
        {
            var supplier = await _context.Suppliers.FirstOrDefaultAsync(x => x.Id == id);
            if (supplier == null)
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }
            return new BaseResponseModel<Supplier>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Success",
                Data = supplier
            };
        }

        public async Task<BaseResponseModel<List<Supplier>>> GetListAsync(SearchSupplierDto input)
        {
            var query = _context.Suppliers.AsQueryable().Where(x => x.Status == Status.ACTIVE.GetHashCode());
            if (!string.IsNullOrEmpty(input.Name))
            {
                query = query.Where(x => x.Name == input.Name);
            }
            if (!string.IsNullOrEmpty(input.Code))
            {
                query = query.Where(x => x.Code == input.Code);
            }

            var data = await query.OrderByDescending(x => x.Id)
            .Skip((input.PageNumber) * input.PageSize)
            .Take(input.PageSize)
            .ToListAsync();

            return new BaseResponseModel<List<Supplier>>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Data = data
            };
        }

        public async Task<BaseResponseModel<Supplier>> UpdateAsync(int id, CreateUpdateSupplierInputDto input)
        {
            var supplier = _context.Suppliers.FirstOrDefault(x => x.Id == id);
            if (supplier == null)
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Không tồn tại bản ghi"
                };
            }

            if (_context.Suppliers.Any(x => x.Name.ToLower() == input.Name.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với tên {input.Name}"
                };
            }

            if (_context.Suppliers.Any(x => x.Code.ToLower() == input.Code.ToLower() && x.Id != id))
            {
                return new BaseResponseModel<Supplier>
                {
                    Code = CodeResponse.Error.GetHashCode(),
                    Message = $"Đã tồn tại bản ghi với mã {input.Code}"
                };
            }

            supplier.Code = input.Code;
            supplier.Name = input.Name;
            supplier.Status = Status.ACTIVE.GetHashCode();

            await _context.SaveChangesAsync();
            return new BaseResponseModel<Supplier>
            {
                Code = CodeResponse.Success.GetHashCode(),
                Message = "Cập nhật bản ghi thành công",
                Data = supplier
            };
        }
    }
}
