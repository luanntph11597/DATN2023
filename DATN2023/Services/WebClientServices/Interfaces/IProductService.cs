﻿using DATN2023.Dtos.ProductDtos;
using System.Collections.Generic;

namespace DATN2023.Services.WebClientServices.Interfaces
{
    public interface IProductService
    {
        public List<ProductDto> SearchProducts(string keyword, decimal? minPrice, decimal? maxPrice, ProductSearchInputDto input);
    }
}
