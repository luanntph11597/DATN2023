﻿using System.Collections.Generic;
using System;
using DATN2023.Services.WebClientServices.Interfaces;
using System.Linq;
using DATN2023.Repositories;
using DATN2023.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;
using System.Drawing;
using Color = DATN2023.Models.Color;
using Size = DATN2023.Models.Size;
using System.Reflection;
using DATN2023.Dtos.ProductDtos;

namespace DATN2023.Services.WebClientServices.Implements
{
    public class ProductService : IProductService
    {
        private readonly ProductRepository _productRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly BrandRepository _brandRepository;
        private readonly ColorRepository _colorRepository;
        private readonly SizeRepository _sizeRepository;
        private readonly SupplierRepository _supplierRepository;
        private readonly GenderRepository _genderRepository;
        private readonly ProductTypeRepository _productTypeRepository;
        private readonly MarterialRepository _marterialRepository;

        public ProductService(ProductRepository productRepository, CategoryRepository categoryRepository, BrandRepository brandRepository
            , ColorRepository colorRepository, SizeRepository sizeRepository, SupplierRepository supplierRepository, GenderRepository genderRepository,
            ProductTypeRepository productTypeRepository, MarterialRepository marterialRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _brandRepository = brandRepository;
            _colorRepository = colorRepository;
            _sizeRepository = sizeRepository;
            _supplierRepository = supplierRepository;
            _genderRepository = genderRepository;
            _productTypeRepository = productTypeRepository;
            _marterialRepository = marterialRepository;
        }

        public List<ProductDto> SearchProducts(string keyword, decimal? minPrice, decimal? maxPrice, ProductSearchInputDto input)
        {
            if (minPrice.HasValue && maxPrice.HasValue && minPrice.Value > maxPrice.Value)
            {
                throw new ArgumentException("Min price should not be greater than max price.");
            }

            var products = _productRepository.Search(keyword, minPrice, maxPrice, input);

            List<Category> category = _categoryRepository.findAll();
            
             List<Brand> brand = _brandRepository.findAll();
            
             List<Color> color = _colorRepository.findAll();
            
          
             List<Size>  size = _sizeRepository.findAll();


            List<Supplier> supplier = _supplierRepository.findAll();

            List<Gender> gender = _genderRepository.findAll();

            List<ProductType> productType = _productTypeRepository.findAll();


            List<Marterial> marterial = _marterialRepository.findAll();
            
            var productDtos = products.Select(
                p => new ProductDto
                {
                Name = p.Name,
                Description = p.Description,
                Price = p.Price,
                CategoryId = p.CategoryId,
                BrandId = p.BrandId,
                ColorId = p.ColorId,
                SizeId = p.SizeId,
                SupplierId = p.SupplierId,
                GenderId = p.GenderId,
                ProductTypeId = p.ProductTypeId,
                MarterialId = p.MarterialId,
                AverageRating = p.AverageRating,
                Status = p.Status,
                Category = category.FirstOrDefault(x => x.Id == p.CategoryId),
                Color = color.FirstOrDefault(x => x.Id == p.ColorId),
                Brand = brand.FirstOrDefault(x => x.Id == p.BrandId),
                Size = size.FirstOrDefault(x => x.Id == p.SizeId),
                Supplier = supplier.FirstOrDefault(x => x.Id == p.SupplierId),
                Gender = gender.FirstOrDefault(x => x.Id == p.GenderId),
                ProductType = productType.FirstOrDefault(x => x.Id == p.ProductTypeId),
                Marterial = marterial.FirstOrDefault(x => x.Id == p.MarterialId)
                }
                ).ToList();
           
            return productDtos;
        }
    }
}
