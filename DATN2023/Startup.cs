using DATN2022.Profiles;

using DATN2023.DBContext;
using DATN2023.Repositories;
using DATN2023.Services.BackOfficeServices.Implements;
using DATN2023.Services.BackOfficeServices.Interfaces;
using DATN2023.Services.WebClientServices.Implements;
using DATN2023.Services.WebClientServices.Interfaces;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using System.IO;

namespace DATN2023
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string filePath = @"D:\DATN2023ConnectionString\connection.txt";
            string connectionString = File.ReadAllText(filePath);
            string requiredString = connectionString.Substring(0);
            services.AddDbContext<DATN2023DBContext>(options =>
            options.UseSqlServer(requiredString));

            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<ISizeService, SizeService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IMarterialService, MarterialService>();
            services.AddTransient<ISupplierService, SupplierService>();
            services.AddTransient<IColorService, ColorService>();


            services.AddTransient<BrandRepository>();
            services.AddTransient<CategoryRepository>();
            services.AddTransient<ColorRepository>();
            services.AddTransient<GenderRepository>();
            services.AddTransient<MarterialRepository>();
            services.AddTransient<ProductTypeRepository>();
            services.AddTransient<ProductRepository>();
            services.AddTransient<SizeRepository>();
            services.AddTransient<SupplierRepository>();

            services.AddAutoMapper(typeof(AutoMapperProfileConfiguration));

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DATN2023", Version = "v1" });
            });
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(policy =>
                {
                    policy.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DATN2023 v1"));
                app.UseCors();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
