﻿using DATN2023.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Xml.Linq;

namespace DATN2023.DBContext
{
    public class DATN2023DBContext : DbContext
    {
        // DbSet cho từng model
        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<ShoppingCart> ShoppingCarts { get; set; }
        public DbSet<ShoppingCartDetail> ShoppingCartDetails { get; set; }
        public DbSet<ShippingInfo> ShippingInfos { get; set; }
        public DbSet<Color> Colors { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Marterial> Marterials { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }

        public DATN2023DBContext(DbContextOptions<DATN2023DBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Áo" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Category { Id = 2, Name = "Quần" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<Brand>().HasData(
                new Brand { Id = 1, Name = "Niek", Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Brand { Id = 2, Name = "Adodop" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Brand { Id = 3, Name = "Buma" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<Color>().HasData(
                new Color { Id = 1, Name = "Đen" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Color { Id = 2, Name = "Bạc" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Color { Id = 3, Name = "Trắng" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<Size>().HasData(
                new Size { Id = 1, Name = "S" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Size { Id = 2, Name = "M" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Size { Id = 3, Name = "L" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );
            modelBuilder.Entity<Gender>().HasData(
            new Size { Id = 1, Name = "Nam" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
            new Size { Id = 2, Name = "Nữ" , Status = Enumirations.Status.ACTIVE.GetHashCode() },
            new Size { Id = 3, Name = "Bê đê nữ thích đồ nam" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
        );

            modelBuilder.Entity<Supplier>().HasData(
                new Supplier { Id = 1, Name = "FPT Fashion" ,Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Supplier { Id = 2, Name = "Fei xu xeng" ,Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Supplier { Id = 3, Name = "Xhon Xhizna" , Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<Image>().HasData(
                new Image { Id = 1, AltValue = "Product Image 1", ImageUrl = "https://example.com/images/product1.jpg", Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 1 },
                new Image { Id = 2, AltValue = "Product Image 2", ImageUrl = "https://example.com/images/product2.jpg", Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 1 },
                new Image { Id = 3, AltValue = "Product Image 3", ImageUrl = "https://example.com/images/product3.jpg", Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 2 },
                new Image { Id = 4, AltValue = "Product Image 4", ImageUrl = "https://example.com/images/product4.jpg", Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 3 }
           );

            modelBuilder.Entity<Product>().HasData(
                new Product {Id = 1, Name = "Áo khoác nam", Description = "Áo khoác nam thời trang, chất liệu vải cao cấp", Price = 500000, ProductScore = 80, AverageRating = 4.5, Quantity = 100, Status = Enumirations.Status.ACTIVE.GetHashCode(), CategoryId = 1, BrandId = 1, ColorId = 1, SizeId = 1, SupplierId = 1 ,GenderId =1, ProductTypeId = 1, MarterialId = 2},
                new Product {Id = 2,  Name = "Quần jean nữ", Description = "Quần jean nữ form rộng, thời trang và thoải mái", Price = 350000, ProductScore = 75, AverageRating = 4.2, Quantity = 50, Status = Enumirations.Status.ACTIVE.GetHashCode(), CategoryId = 2, BrandId = 2,  ColorId = 2, SizeId = 2, SupplierId = 2 , GenderId = 1, ProductTypeId = 3, MarterialId = 1},
                new Product {Id = 3, Name = "Quần jean Bê đê", Description = "Quần jean bê đê form rộng, diêm dúa và màu sắc", Price = 950000, ProductScore = 75, AverageRating = 4.2, Quantity = 50, Status = Enumirations.Status.ACTIVE.GetHashCode(), CategoryId = 2, BrandId = 2,  ColorId = 2, SizeId = 2, SupplierId = 2 , GenderId = 2, ProductTypeId = 2, MarterialId = 2 }
            );

            modelBuilder.Entity<Customer>().HasData(
                new Customer { Id = 1, FullName = "John Doe", Email = "johndoe@example.com", Password = "password123", Address = "123 Main St, Anytown USA", PhoneNumber = "1234567890", Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Customer { Id = 2, FullName = "Jane Smith", Email = "janesmith@example.com", Password = "password456", Address = "456 Oak St, Anytown USA", PhoneNumber = "2345678901", Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new Customer { Id = 3, FullName = "Bob Johnson", Email = "bobjohnson@example.com", Password = "password789", Address = "789 Maple St, Anytown USA", PhoneNumber = "3456789012", Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<Order>().HasData(
                new Order { Id = 1, OrderDate = DateTime.Now, TotalPrice = 100, OrderStatus = "Pending", Note = "Please deliver before 5pm", PaymentMethodId = 1, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 1 },
                new Order { Id = 2, OrderDate = DateTime.Now.AddDays(-1), TotalPrice = 200, OrderStatus = "Shipped", Note = "", PaymentMethodId = 2, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 2 },
                new Order { Id = 3, OrderDate = DateTime.Now.AddDays(-2), TotalPrice = 300, OrderStatus = "Delivered", Note = "Received by John", PaymentMethodId = 3, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 3 }
            );

            modelBuilder.Entity<OrderDetail>().HasData(
                new OrderDetail { Id = 1, Quantity = 2, UnitPrice = 10.99m, Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 1, ProductId = 1 },
                new OrderDetail { Id = 2, Quantity = 1, UnitPrice = 19.99m, Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 1, ProductId = 2 },
                new OrderDetail { Id = 3, Quantity = 3, UnitPrice = 7.5m, Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 2, ProductId = 1 },
                new OrderDetail { Id = 4, Quantity = 2, UnitPrice = 5.99m, Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 2, ProductId = 3 },
                new OrderDetail { Id = 5, Quantity = 1, UnitPrice = 8.99m, Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 3, ProductId = 2 }
            );

            modelBuilder.Entity<PaymentMethod>().HasData(
                new PaymentMethod { Id = 1, Name = "Credit Card", BankName = "ABC Bank", AccountNumber = "123456789", Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new PaymentMethod { Id = 2, Name = "PayPal", BankName = "PayPal", AccountNumber = "abc@example.com", Status = Enumirations.Status.ACTIVE.GetHashCode() },
                new PaymentMethod { Id = 3, Name = "Cash on Delivery", BankName = "", AccountNumber = "", Status = Enumirations.Status.ACTIVE.GetHashCode() }
            );

            modelBuilder.Entity<ShippingInfo>().HasData(
                new ShippingInfo { Id = 1, Address = "123 Main St, Anytown USA", PhoneNumber = "123-456-7890", DeliveryDate = DateTime.Parse("2023-03-01"), ShippingStatus = "Preparing to ship", Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 1 },
                new ShippingInfo { Id = 2, Address = "456 Oak St, Anytown USA", PhoneNumber = "555-555-5555", DeliveryDate = DateTime.Parse("2023-03-02"), ShippingStatus = "Shipped", Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 2 },
                new ShippingInfo { Id = 3, Address = "789 Maple St, Anytown USA", PhoneNumber = "999-999-9999", DeliveryDate = DateTime.Parse("2023-03-03"), ShippingStatus = "Delivered", Status = Enumirations.Status.ACTIVE.GetHashCode(), OrderId = 3 }
            );

            modelBuilder.Entity<ShoppingCart>().HasData(
                new ShoppingCart { Id = 1, CreatedDate = DateTime.Now, TotalPrice = 0, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 1 },
                new ShoppingCart { Id = 2, CreatedDate = DateTime.Now, TotalPrice = 0, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 2 },
                new ShoppingCart { Id = 3, CreatedDate = DateTime.Now, TotalPrice = 0, Status = Enumirations.Status.ACTIVE.GetHashCode(), CustomerId = 3 }
            );

            modelBuilder.Entity<ShoppingCartDetail>().HasData(
                new ShoppingCartDetail { Id = 1, Quantity = 2, Price = 100.00m, Status = Enumirations.Status.ACTIVE.GetHashCode(), ShoppingCartId = 1, ProductId = 1 },
                new ShoppingCartDetail { Id = 2, Quantity = 1, Price = 50.00m, Status = Enumirations.Status.ACTIVE.GetHashCode(), ShoppingCartId = 1, ProductId = 2 },
                new ShoppingCartDetail { Id = 3, Quantity = 3, Price = 75.00m, Status = Enumirations.Status.ACTIVE.GetHashCode(), ShoppingCartId = 2, ProductId = 1 }
            );

            modelBuilder.Entity<ProductReview>().HasData(
                new ProductReview { Id = 1, Content = "Sản phẩm rất tốt", Rating = 5, Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 1, CustomerId = 1 },
                new ProductReview { Id = 2, Content = "Sản phẩm không tốt lắm", Rating = 2, Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 1, CustomerId = 2 },
                new ProductReview { Id = 3, Content = "Sản phẩm bình thường", Rating = 3, Status = Enumirations.Status.ACTIVE.GetHashCode(), ProductId = 1, CustomerId = 3 }
            );
            modelBuilder.Entity<Marterial>().HasData(
               new Marterial { Id =  1, Name = "Cotton", Description = "Soft and comfortable", Status = Enumirations.Status.ACTIVE.GetHashCode() },
               new Marterial { Id = 2, Name = "Leather", Description = "Durable and stylish", Status = Enumirations.Status.ACTIVE.GetHashCode() },
               new Marterial { Id = 3, Name = "Wool", Description = "Warm and cozy", Status = Enumirations.Status.ACTIVE.GetHashCode() }
           );
            modelBuilder.Entity<ProductType>().HasData(
               new ProductType { Id = 1, Name = "Áo thun", Description = "Loại áo được làm từ chất liệu thun co giãn, thường mặc ở nhà hoặc đi chơi, thể thao.", Status = Enumirations.Status.ACTIVE.GetHashCode() },
               new ProductType { Id = 2, Name = "Áo sơ mi", Description = "Loại áo được may từ vải cotton, tơ tằm, lụa... thường mặc ở nơi công sở, đi tiệc, dự lễ hội...", Status = Enumirations.Status.ACTIVE.GetHashCode() },
               new ProductType { Id = 3, Name = "Quần jean", Description = "Loại quần được làm từ chất liệu vải jean (denim), có nhiều kiểu dáng và màu sắc phù hợp cho nhiều hoàn cảnh.", Status = Enumirations.Status.ACTIVE.GetHashCode() }
           );
            base.OnModelCreating(modelBuilder);
        }
    }
}
