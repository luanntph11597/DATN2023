﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;

namespace DATN2023.Models
{
    public class Product
    {
        [Key]
        //Mã số duy nhất cho mỗi sản phẩm.
        public int Id { get; set; }

        //Tên của sản phẩm.
        public string Name { get; set; }

        //Mô tả chi tiết sản phẩm.
        public string Description { get; set; }

        //Giá tiền của sản phẩm.
        public decimal Price { get; set; }

        //Điểm sản phẩm giúp người bán dựa vào để cải thiện bài đăng sản phẩm.
        public int ProductScore { get; set; }

        //Điểm đánh giá trung bình của khách hàng.
        public double AverageRating { get; set; }

        //Số lượng sản phẩm có sẵn.
        public int Quantity { get; set; }

        //Trạng thái của bản ghi.
        public int Status { get; set; }

        //Khóa ngoại đến bảng Thể loại sản phẩm.
        public int CategoryId { get; set; }

        //Thể loại sản phẩm tương ứng với sản phẩm.
        public Category Category { get; set; }

        //Khóa ngoại đến bảng Thương hiệu sản phẩm.
        public int BrandId { get; set; }

        //Thương hiệu sản phẩm tương ứng với sản phẩm.
        public Brand Brand { get; set; }

        //Khóa ngoại đến bảng Màu sắc.
        public int ColorId { get; set; }

        //Màu sắc tương ứng với sản phẩm.
        public Color Color { get; set; }

        //Khóa ngoại đến bảng Kích cỡ.
        public int SizeId { get; set; }

        //Kích cỡ tương ứng với sản phẩm.
        public Size Size { get; set; }

        //Khóa ngoại đến bảng Nhà cung cấp.
        public int SupplierId { get; set; }

        // Nhà cung cấp tương ứng với sản phẩm.
        public Supplier Supplier { get; set; }

        //Khóa ngoại đến bảng Giới tính.
        public int GenderId { get; set; }

        // Giới tính tương ứng với sản phẩm.
        public Gender Gender { get; set; }

        // Khóa ngoại đến bảng Kiểu loại sản phẩm.
        public int ProductTypeId { get; set; }

        // Kiểu loại sản phẩm tương ứng với chất liệu sản phẩm.
        public ProductType ProductType { get; set; }

        // Khóa ngoại đến bảng Chất liệu.
        public int MarterialId { get; set; }

        // Chất liệu sản phẩm tương ứng.
        public Marterial Marterial { get; set; }
    }
}
