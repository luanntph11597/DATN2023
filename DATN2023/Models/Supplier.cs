﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Supplier
    {
        [Key]
        // Mã số duy nhất cho mỗi nhà cung cấp.
        public int Id { get; set; }

        // Mã của nhà cung cấp.
        public string Code { get; set; }

        // Tên gọi của nhà cung cấp.
        public string Name { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
