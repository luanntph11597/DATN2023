﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class ProductReview
    {
        [Key]
        // Mã số duy nhất cho mỗi đánh giá.
        public int Id { get; set; }

        // Nội dung đánh giá của khách hàng.
        public string Content { get; set; }

        // Điểm đánh giá.
        public int Rating { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Sản phẩm.
        public int ProductId { get; set; }

        // Đối tượng tham chiếu đến bảng Sản phẩm.
        public Product Product { get; set; }

        // Khóa ngoại đến bảng Khách hàng.
        public int CustomerId { get; set; }

        // Đối tượng tham chiếu đến bảng Khách hàng.
        public Customer Customer { get; set; }
    }
}
