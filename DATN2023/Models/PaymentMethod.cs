﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class PaymentMethod
    {
        // Mã số duy nhất cho mỗi phương thức thanh toán.
        [Key]
        public int Id { get; set; }

        // Tên gọi của phương thức thanh toán.
        public string Name { get; set; }

        // Tên ngân hàng mà phương thức thanh toán sử dụng.
        public string BankName { get; set; }

        // Số tài khoản được liên kết với phương thức thanh toán.
        public string AccountNumber { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
