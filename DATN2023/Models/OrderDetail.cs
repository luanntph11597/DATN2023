﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class OrderDetail
    {
        [Key]
        // Mã số duy nhất cho mỗi chi tiết đơn hàng.
        public int Id { get; set; }

        // Số lượng sản phẩm được đặt hàng.
        public int Quantity { get; set; }

        // Giá tiền của sản phẩm khi được đặt hàng.
        public decimal UnitPrice { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Đơn hàng.
        public int OrderId { get; set; }

        // Đối tượng tham chiếu đến bảng Đơn hàng.
        public Order Order { get; set; }

        // Khóa ngoại đến bảng Sản phẩm.
        public int ProductId { get; set; }

        // Đối tượng tham chiếu đến bảng Sản phẩm.
        public Product Product { get; set; }
    }
}
