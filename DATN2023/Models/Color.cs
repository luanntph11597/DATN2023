﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Color
    {
        [Key]
        // Mã số duy nhất cho mỗi màu sắc.
        public int Id { get; set; }

        // Mã màu (Hex)
        public string Code { get; set; }

        // Tên gọi của màu sắc.
        public string Name { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
