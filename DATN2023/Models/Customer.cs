﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Customer
    {
        [Key]
        // Mã số duy nhất cho mỗi khách hàng.
        public int Id { get; set; }

        // Tên đầy đủ của khách hàng.
        public string FullName { get; set; }

        // Địa chỉ email của khách hàng.
        public string Email { get; set; }

        // Mật khẩu của khách hàng.
        public string Password { get; set; }

        // Địa chỉ khách hàng.
        public string Address { get; set; }

        // Số điện thoại của khách hàng.
        public string PhoneNumber { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
