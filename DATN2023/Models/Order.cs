﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Order
    {
        [Key]
        // Mã số duy nhất cho mỗi đơn hàng.
        public int Id { get; set; }

        // Ngày đặt hàng.
        public DateTime OrderDate { get; set; }

        // Tổng số tiền của đơn hàng.
        public decimal TotalPrice { get; set; }

        // Tình trạng của đơn hàng.
        public string OrderStatus { get; set; }

        // Ghi chú thêm về đơn hàng.
        public string Note { get; set; }


        // Khóa ngoại đến bảng Phương thức thanh toán.
        public int PaymentMethodId { get; set; }

        // Đối tượng tham chiếu đến bảng Phương thức thanh toán.
        public PaymentMethod PaymentMethod { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Khách hàng.
        public int CustomerId { get; set; }

        // Đối tượng tham chiếu đến bảng Khách hàng.
        public Customer Customer { get; set; }

        // Đối tượng tham chiếu đến bảng thông tin vận chuyển.
        public ShippingInfo ShippingInfo { get; set; }
    }
}
