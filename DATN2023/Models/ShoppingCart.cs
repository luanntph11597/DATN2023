﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class ShoppingCart
    {
        [Key]
        // Mã số duy nhất cho mỗi giỏ hàng.
        public int Id { get; set; }

        // Ngày tạo giỏ hàng.
        public DateTime CreatedDate { get; set; }

        // Tổng số tiền của giỏ hàng.
        public decimal TotalPrice { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Khách hàng.
        public int CustomerId { get; set; }

        // Đối tượng tham chiếu đến bảng Khách hàng.
        public Customer Customer { get; set; }
    }
}
