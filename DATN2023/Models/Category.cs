﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Category
    {
        [Key]
        //Mã số duy nhất cho mỗi thể loại sản phẩm.
        public int Id { get; set; }

        //Mã của thể loại sản phẩm.
        public string Code { get; set; }

        //Tên của thể loại sản phẩm.
        public string Name { get; set; }

        //Mô tả chi tiết về thể loại sản phẩm.
        public string Description { get; set; }

        //Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
