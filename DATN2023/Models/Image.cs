﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Image
    {

        [Key]
        // Mã số duy nhất cho mỗi hình ảnh.
        public int Id { get; set; }

        // Thông tin hình ảnh.
        public string AltValue { get; set; }

        // Hình ảnh sản phẩm.
        public string ImageUrl { get; set; }

        // Trạng thái hình ảnh.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Sản phẩm.
        public int ProductId { get; set; }

        public Product Product { get; set; }
    }
}
