﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class ShippingInfo
    {
        [Key]
        // Mã số duy nhất cho mỗi thông tin vận chuyển.
        public int Id { get; set; }

        // Địa chỉ nhận hàng.
        public string Address { get; set; }

        // Số điện thoại người nhận hàng.
        public string PhoneNumber { get; set; }

        // Ngày dự kiến giao hàng.
        public DateTime DeliveryDate { get; set; }

        // Tình trạng vận chuyển của đơn hàng.
        public string ShippingStatus { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Đơn hàng.
        public int OrderId { get; set; }

        // Đối tượng tham chiếu đến bảng Đơn hàng.
        public Order Order { get; set; }

    }
}
