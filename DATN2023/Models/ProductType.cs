﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    //Kiểu sản phẩm ( ngắn tay, dài tay,..)
    public class ProductType
    {
        [Key]
        // Mã số duy nhất cho mỗi kiểu loại sản phẩm.
        public int Id { get; set; }

        // Mã kiểu loại sản phẩm
        public string Code { get; set; }

        // Tên của kiểu loại sản phẩm.
        public string Name { get; set; }

        // Mô tả chi tiết về kiểu loại sản phẩm.
        public string Description { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
