﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Gender
    {
        [Key]
        // Mã số duy nhất cho mỗi giới tính
        public int Id { get; set; } 
        // Tên giới tính ( nam, nữ)
        public string Name { get; set; } 
        // Trạng thái của bản ghi (ví dụ: Active, Inactive)
        public int Status { get; set; } 
    }
}
