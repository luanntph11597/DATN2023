﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Brand
    {
        [Key]
        // Mã số duy nhất cho mỗi thương hiệu.
        public int Id { get; set; }

        // Tên gọi của thương hiệu.
        public string Name { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
