﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class ShoppingCartDetail
    {
        [Key]
        // Mã số duy nhất cho mỗi chi tiết giỏ hàng.
        public int Id { get; set; }

        // Số lượng sản phẩm trong giỏ hàng.
        public int Quantity { get; set; }

        // Giá tiền của sản phẩm trong giỏ hàng.
        public decimal Price { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

        // Khóa ngoại đến bảng Giỏ hàng.
        public int ShoppingCartId { get; set; }

        // Đối tượng tham chiếu đến bảng Giỏ hàng.
        public ShoppingCart ShoppingCart { get; set; }

        // Khóa ngoại đến bảng Sản phẩm.
        public int ProductId { get; set; }

        // Đối tượng tham chiếu đến bảng Sản phẩm.
        public Product Product { get; set; }
    }
}
