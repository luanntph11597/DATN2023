﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    public class Size
    {

        [Key]
        // Mã số duy nhất cho mỗi kích cỡ.
        public int Id { get; set; }

        // Mã size
        public string Code { get; set; }

        // Tên gọi của kích cỡ.
        public string Name { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }
    }
}
