﻿using System.ComponentModel.DataAnnotations;

namespace DATN2023.Models
{
    // Chất liệu sản phẩm(nhung, thổ cẩm ,...)
    public class Marterial
    {
        [Key]
        // Mã số duy nhất cho mỗi loại chất liệu sản phẩm.
        public int Id { get; set; }

        // Mã của chất liệu sản phẩm.
        public string Code { get; set; }

        // Tên của chất liệu sản phẩm.
        public string Name { get; set; }

        // Mô tả chi tiết về chất liệu sản phẩm.
        public string Description { get; set; }

        // Trạng thái của bản ghi.
        public int Status { get; set; }

    }
}
