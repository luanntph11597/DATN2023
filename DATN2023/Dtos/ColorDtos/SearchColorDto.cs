﻿using DATN2023.BaseModel;

namespace DATN2023.Dtos.ColorDtos
{
    public class SearchColorDto : PageModel
    {
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
    }
}
