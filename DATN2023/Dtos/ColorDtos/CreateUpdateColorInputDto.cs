﻿namespace DATN2023.Dtos.ColorDtos
{
    public class CreateUpdateColorInputDto
    {
        public string ColorName { get; set; }
        public string ColorCode { get; set; }
    }
}
