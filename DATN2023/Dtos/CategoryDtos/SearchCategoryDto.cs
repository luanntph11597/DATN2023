﻿using DATN2023.BaseModel;

namespace DATN2023.Dtos.CategoryDtos
{
    public class SearchCategoryDto : PageModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
