﻿namespace DATN2023.Dtos.CategoryDtos
{
    public class CreateUpdateCategoryInputDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
