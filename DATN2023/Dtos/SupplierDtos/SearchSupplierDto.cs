﻿using DATN2023.BaseModel;

namespace DATN2023.Dtos.SupplierDtos
{
    public class SearchSupplierDto : PageModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
