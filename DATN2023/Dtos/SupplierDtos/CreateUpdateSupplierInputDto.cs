﻿namespace DATN2023.Dtos.SupplierDtos
{
    public class CreateUpdateSupplierInputDto
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
