﻿using DATN2023.BaseModel;

namespace DATN2023.Dtos.MarterialDtos
{
    public class SearchMarterialDto : PageModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
