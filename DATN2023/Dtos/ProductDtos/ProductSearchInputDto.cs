﻿namespace DATN2023.Dtos.ProductDtos
{
    public class ProductSearchInputDto
    {
        //Điểm đánh giá trung bình của khách hàng.
        public double? AverageRating { get; set; }

        //Trạng thái của bản ghi.
        public int? Status { get; set; }

        //Khóa ngoại đến bảng Thể loại sản phẩm.
        public int? CategoryId { get; set; }

        //Khóa ngoại đến bảng Thương hiệu sản phẩm.
        public int? BrandId { get; set; }

        //Khóa ngoại đến bảng Màu sắc.
        public int? ColorId { get; set; }

        //Khóa ngoại đến bảng Kích cỡ.
        public int? SizeId { get; set; }

        //Khóa ngoại đến bảng Nhà cung cấp.
        public int? SupplierId { get; set; }

        //Khóa ngoại đến bảng Giới tính.
        public int? GenderId { get; set; }

        // Khóa ngoại đến bảng Chất liệu.
        public int? MarterialId { get; set; }

        // Khóa ngoại đến bảng Kiểu loại sản phẩm.
        public int? ProductTypeId { get; set; }
    }
}
