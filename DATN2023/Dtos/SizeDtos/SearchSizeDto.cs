﻿using DATN2023.BaseModel;

using Microsoft.AspNetCore.Mvc.RazorPages;

using PageModel = DATN2023.BaseModel.PageModel;

namespace DATN2023.Dtos.SizeDtos
{
    public class SearchSizeDto : PageModel
    {
        public string SizeCode { get; set; }
        public string SizeName { get; set; }
    }
}
