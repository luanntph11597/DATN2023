﻿namespace DATN2023.Dtos.SizeDtos
{
    public class CreateUpdateSizeInputDto
    {
        //tên kích cỡ
        public string SizeName { get; set; }

        //mã kích cỡ
        public string SizeCode { get; set; }
    }
}
