﻿namespace DATN2023.Enumirations
{
    public enum Status
    {
        ACTIVE = 1,
        INACTIVE = 0,
        DELETED = -1,
    }
}
