﻿using AutoMapper;
using DATN2023.Dtos.ProductDtos;
using DATN2023.Models;

namespace DATN2022.Profiles
{
    public class AutoMapperProfileConfiguration:Profile
    {
        public AutoMapperProfileConfiguration()
        {
            CreateMap<ProductDto,Product>();
           
        }
    }
}
