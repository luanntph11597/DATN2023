﻿using DATN2023.BaseModel;
using DATN2023.Dtos.ColorDtos;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

using System.Threading.Tasks;

namespace DATN2023.Controllers.BackOfficeControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColorController : ControllerBase
    {
        private readonly IColorService _colorService;
        public ColorController(IColorService colorService)
        {
            _colorService = colorService;
        }

        [HttpPost]
        public async Task<BaseResponseModel<Color>> CreateAsync([FromBody] CreateUpdateColorInputDto input)
        {
            return await _colorService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<BaseResponseModel<Color>> UpdateAsync(int id, [FromBody] CreateUpdateColorInputDto input)
        {
            return await _colorService.UpdateAsync(id, input);
        }

        [HttpPost("getlist")]
        public async Task<BaseResponseModel<List<Color>>> GetListAsync(SearchColorDto input)
        {
            return await _colorService.GetListAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<BaseResponseModel<Color>> DeleteAsync(int id)
        {
            return await _colorService.DeleteAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<Color>> GetAsync(int id)
        {
            return await _colorService.GetAsync(id);
        }
    }
}
