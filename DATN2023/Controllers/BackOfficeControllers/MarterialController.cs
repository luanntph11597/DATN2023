﻿using DATN2023.BaseModel;
using DATN2023.Dtos.MarterialDtos;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

using System.Threading.Tasks;

namespace DATN2023.Controllers.BackOfficeControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarterialController : ControllerBase
    {
        private readonly IMarterialService _marterialService;
        public MarterialController(IMarterialService marterialService)
        {
            _marterialService = marterialService;
        }

        [HttpPost]
        public async Task<BaseResponseModel<Marterial>> CreateAsync([FromBody] CreateUpdateMarterialInputDto input)
        {
            return await _marterialService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<BaseResponseModel<Marterial>> UpdateAsync(int id, [FromBody] CreateUpdateMarterialInputDto input)
        {
            return await _marterialService.UpdateAsync(id, input);
        }

        [HttpPost("getlist")]
        public async Task<BaseResponseModel<List<Marterial>>> GetListAsync(SearchMarterialDto input)
        {
            return await _marterialService.GetListAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<BaseResponseModel<Marterial>> DeleteAsync(int id)
        {
            return await _marterialService.DeleteAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<Marterial>> GetAsync(int id)
        {
            return await _marterialService.GetAsync(id);
        }
    }
}
