﻿using DATN2023.BaseModel;
using DATN2023.Dtos.SizeDtos;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace DATN2023.Controllers.BackOfficeControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SizeController : ControllerBase
    {
        private readonly ISizeService _sizeService;

        public SizeController(ISizeService sizeService)
        {
            _sizeService = sizeService;
        }

        [HttpPost]
        public async Task<BaseResponseModel<Size>> CreateAsync([FromBody] CreateUpdateSizeInputDto input)
        {
            return await _sizeService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<BaseResponseModel<Size>> UpdateAsync(int id, [FromBody] CreateUpdateSizeInputDto input)
        {
            return await _sizeService.UpdateAsync(id, input);
        }

        [HttpPost("getlist")]
        public async Task<BaseResponseModel<List<Size>>> GetListAsync(SearchSizeDto input)
        {
           return await _sizeService.GetListAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<BaseResponseModel<Size>> DeleteAsync(int id)
        {
            return await _sizeService.DeleteAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<Size>> GetAsync(int id)
        {
            return await _sizeService.GetAsync(id);
        }
    }
}
