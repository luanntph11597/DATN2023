﻿using DATN2023.BaseModel;
using DATN2023.Dtos.SupplierDtos;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

using System.Threading.Tasks;

namespace DATN2023.Controllers.BackOfficeControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService _supplierService;
        public SupplierController(ISupplierService supplierService)
        {
            _supplierService = supplierService;
        }

        [HttpPost]
        public async Task<BaseResponseModel<Supplier>> CreateAsync([FromBody] CreateUpdateSupplierInputDto input)
        {
            return await _supplierService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<BaseResponseModel<Supplier>> UpdateAsync(int id, [FromBody] CreateUpdateSupplierInputDto input)
        {
            return await _supplierService.UpdateAsync(id, input);
        }

        [HttpPost("getlist")]
        public async Task<BaseResponseModel<List<Supplier>>> GetListAsync(SearchSupplierDto input)
        {
            return await _supplierService.GetListAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<BaseResponseModel<Supplier>> DeleteAsync(int id)
        {
            return await _supplierService.DeleteAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<Supplier>> GetAsync(int id)
        {
            return await _supplierService.GetAsync(id);
        }
    }
}
