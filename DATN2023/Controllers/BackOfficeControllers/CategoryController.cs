﻿using DATN2023.BaseModel;
using DATN2023.Dtos.CategoryDtos;
using DATN2023.Dtos.ColorDtos;
using DATN2023.Models;
using DATN2023.Services.BackOfficeServices.Interfaces;

using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;

using System.Threading.Tasks;

namespace DATN2023.Controllers.BackOfficeControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost]
        public async Task<BaseResponseModel<Category>> CreateAsync([FromBody] CreateUpdateCategoryInputDto input)
        {
            return await _categoryService.CreateAsync(input);
        }

        [HttpPut("{id}")]
        public async Task<BaseResponseModel<Category>> UpdateAsync(int id, [FromBody] CreateUpdateCategoryInputDto input)
        {
            return await _categoryService.UpdateAsync(id, input);
        }

        [HttpPost("getlist")]
        public async Task<BaseResponseModel<List<Category>>> GetListAsync(SearchCategoryDto input)
        {
            return await _categoryService.GetListAsync(input);
        }

        [HttpDelete("{id}")]
        public async Task<BaseResponseModel<Category>> DeleteAsync(int id)
        {
            return await _categoryService.DeleteAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<BaseResponseModel<Category>> GetAsync(int id)
        {
            return await _categoryService.GetAsync(id);
        }
    }
}
