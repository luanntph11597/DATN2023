﻿using DATN2023.Dtos.ProductDtos;
using DATN2023.Services.WebClientServices.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace DATN2023.Controllers.WebClientControllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class WCProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public WCProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost]
        public IActionResult SearchProducts(string keyword, decimal? minPrice, decimal? maxPrice, ProductSearchInputDto input)
        {
            var products = _productService.SearchProducts(keyword, minPrice, maxPrice, input);
            return Ok(products);
        }
    }
}
